// Functions

/*Syntax:

	function functionName(){
		code block (statement)
		}
*/

function printName (){
	console.log('My name is Josue')
}
printName();


// Function Declaration vs. Expression

declaredFunction();

function declaredFunction(){
	console.log("Hi I am from declaredFunction")
};

declaredFunction();

// Function Expression

	// Anonymous function - function w/o name

let variableFunction = function(){
	console.log('I am from variableFunction')
};
variableFunction();

let funcExpression = function FuncName(){
	console.log('Hello from the other side')
};
funcExpression();

// You can reassign declared functions and function expression to the neew anonymous function.

declaredFunction = function(){
	console.log("Updated declaredFunction")
};

declaredFunction();

const constantFunction = function(){
	console.log('Initialized with const')
};

constantFunction();

// constantFunction = function(){
// 	console.log('Cannot be reassigned')
// };

// constantFunction();

// Uunction Scoping
/*
	Jave VAriables has 3 types of scope
	1. Local/block scope
	2. global scope
	3.function scope
*/
{
	let localVarr = 'Armando Perez';
}

let globalVar = 'Mr. Josue';
console.log(globalVar);

function showNames(){
		// Function scope
		var functionVar = 'Joe';
		const functionConst = 'Nick';
		let functionLet = 'Kevin';

		console.log (functionVar);
		console.log (functionConst);
		console.log (functionLet)

};
showNames();

		// console.log (functionVar);
		// console.log (functionConst);
		// console.log (functionLet)

// Nested Function

function myNewFunction(){
		let name = 'Yor';

		function nestedFunction(){
				let nestedName = 'Brando';
				console.log(nestedName);
		}	
		nestedFunction();	
};
myNewFunction();

// Function and Global Scoped Variable
let globalName = 'Alan';

function myNewFunction2(){
		letInside = 'Marco';
		console.log(globalName);
}
myNewFunction2();

//alert()

// alert('Hello World');

// function showSampleAlert(){
// 		alert('Hello User!');
// };
// showSampleAlert();

// console.log ('I will only log in the console when the alert is dismissed');

//  promt()
/*
		Syntax:
// 				promt ("<dialog>'')	;	
// */
// let samplePromt = prompt('Enter Your Name:');
// console.log('Hello' + samplePromt);

// let sampleNullPrompt = prompt(' Dont Enter anything');
// console.log(sampleNullPrompt);


// function printWelcomeMessage(){
// 		let firstName = prompt ('Enter your first name:');
// 		let lastName = prompt ('Enter your last name:');

// 	console.log('Hello,' +firstName + "" + lastName + "!");
// 	console.log('Welcome to my Page');
// };

// printWelcomeMessage();

// Function Naming Conventions



function getCourses(){
		let courses = ['Science 101,', 'Arithmetic 103,' , 'Grammar 105'];
		console.log(courses);
};		
getCourses();

// Avoid pointless and inappropriale function names

function displayCarInfo(){
		console.log('Brand: Toyota');
		console.log('Type: Sedan');
		console.log('Price: 1,500,000');
};
displayCarInfo();